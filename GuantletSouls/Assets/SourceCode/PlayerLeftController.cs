﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerLeftController : MonoBehaviour
{
    private Rigidbody2D rb2d;
    private Transform trans;

    public float MovementMultiplier;

    private float fUpperBounds = 3.68f;
    private float fLowerBounds = -3.78f;

    private void Start()
    {
        rb2d = GetComponent<Rigidbody2D>();
        trans = GetComponent<Transform>();
    }

    private void Update()
    {
        //float moveHorizontal = Input.GetAxis("Horizontal") * MovementMultiplier;
        float moveVertical = Input.GetAxis("Vertical") * MovementMultiplier;

        //Vector2 movement = new Vector2(moveHorizontal * MovementMultiplier, moveVertical * MovementMultiplier);
        //rb2d.AddForce(movement);

        if(trans.position.y <= fLowerBounds)
        {
            if (moveVertical > 0.0f)
            {
                MoveObject(moveVertical);
            }
        }
        else if(trans.position.y >= fUpperBounds)
        {
            if(moveVertical < 0.0f)
            {
                MoveObject(moveVertical);
            }
        }
        else if (trans.position.y >= fLowerBounds && trans.position.y <= fUpperBounds)
        {
            MoveObject(moveVertical);
        }
    }

    private void MoveObject(float moveVertical)
    {
        trans.SetPositionAndRotation(
            new Vector3(trans.position.x, trans.position.y + moveVertical),
            new Quaternion(trans.rotation.x, trans.rotation.y, trans.rotation.z, trans.rotation.w));
    }

}
