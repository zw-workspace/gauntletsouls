﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PressStartChecker : MonoBehaviour
{
    public string sceneName;

    // Start is called before the first frame update
    private void Start()
    {
        sceneName = "Level1";
    }
    
    // Update is called once per frame
    void Update()
    {
        if (Input.GetButtonUp("Submit"))
        {
            SceneManager.LoadScene(sceneName);
        }
    }
}
