﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PongBallMovement : MonoBehaviour
{
    private Rigidbody2D rb2d;
    private Transform trans;
    private CircleCollider2D circleCollider2d;

    public float fInitialSpeed;

    private Vector2 randomForce;

    private GameObject gameObject_PaddleLeft;
    private GameObject gameObject_PaddleRight;
    private GameObject gameObject_TopWall;
    private GameObject gameObject_BotWall;

    private PolygonCollider2D PolyCollider2d_PaddleLeft;
    private PolygonCollider2D PolyCollider2d_PaddleRight;
    private BoxCollider2D boxCollider2d_TopWall;
    private BoxCollider2D boxCollider2d_BotWall;

    public AudioClip audioClip_SFXBounce;
    private AudioSource audioSource_SFXBounce;

    private Vector2 vector2_Right;
    private Vector2 vector2_Left;
    private Vector2 vector2_Up;
    private Vector2 vector2_Down;

    private void Start()
    {
        rb2d = GetComponent<Rigidbody2D>();
        trans = GetComponent<Transform>();
        circleCollider2d = GetComponent<CircleCollider2D>();

        audioSource_SFXBounce = GetComponent<AudioSource>();

        gameObject_PaddleLeft = GameObject.Find("PlayerLeftPaddleJoint");
        gameObject_PaddleRight = GameObject.Find("computerRightPaddleJoint");
        gameObject_TopWall = GameObject.Find("TopWall");
        gameObject_BotWall = GameObject.Find("BottomWall");

        audioClip_SFXBounce = Resources.Load("SFX/DankBeepSFX") as AudioClip;

        PolyCollider2d_PaddleLeft = gameObject_PaddleLeft.GetComponent<PolygonCollider2D>();
        PolyCollider2d_PaddleRight = gameObject_PaddleRight.GetComponent<PolygonCollider2D>();
        boxCollider2d_TopWall = gameObject_TopWall.GetComponent<BoxCollider2D>();
        boxCollider2d_BotWall = gameObject_BotWall.GetComponent<BoxCollider2D>();

        rb2d.AddForce( new Vector2(fInitialSpeed, 0f));

        float fMovement = 40f;

        vector2_Right = new Vector2(fMovement, 0f);
        vector2_Left = new Vector2(-fMovement, 0f);

        vector2_Up = new Vector2(0f, fMovement);
        vector2_Down = new Vector2(0f, -fMovement);
    }

    private void Update()
    {
        // Play SFX
        if (circleCollider2d.IsTouching(PolyCollider2d_PaddleLeft) ||
            circleCollider2d.IsTouching(PolyCollider2d_PaddleRight))
        {
            audioSource_SFXBounce.PlayOneShot(audioClip_SFXBounce, 0.05f);
        }

        // If the ball Collides ?
        if (circleCollider2d.IsTouching(PolyCollider2d_PaddleLeft) || 
            circleCollider2d.IsTouching(PolyCollider2d_PaddleRight) ||
            circleCollider2d.IsTouching(boxCollider2d_TopWall) ||
            circleCollider2d.IsTouching(boxCollider2d_BotWall))
        {
            rb2d.AddForce(rb2d.velocity.normalized * -1f);
        }

        if (circleCollider2d.IsTouching(PolyCollider2d_PaddleRight))
        {
            rb2d.AddForce(vector2_Left);
        }

        if (circleCollider2d.IsTouching(PolyCollider2d_PaddleLeft))
        {
            rb2d.AddForce(vector2_Right);
        }

        if(circleCollider2d.IsTouching(boxCollider2d_TopWall))
        {
            rb2d.AddForce(vector2_Down);
        }

        if (circleCollider2d.IsTouching(boxCollider2d_BotWall))
        {
            rb2d.AddForce(vector2_Up);
        }

    }

}

